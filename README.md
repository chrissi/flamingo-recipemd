# flamingo-RecipeMD

This project adds a RecipeMD.org parser to your flaming-web.org static site!

The RecipeParser assumes that all \*.md -files in your
content directory follow the RecipeMD specification
and renders them as contents.
(But this also means that you can not have regular content
in Markdow files!)

The following elements of the recipe are exposed in the
content-object:
* ``title``
* ``tags`` 

Everything else can be accessed using the ``content['recipe']``
attribute.

## How to use

First add this plugin to your ``REQUIREMENTS.txt`` by adding
the following line:
```
git+https://gitli.stratum0.org/chrissi/flamingo-recipemd.git#egg=flamingo-recipemd
```

Then add it to your flaming plugin-list:

```
PLUGINS = [
    # ...
    'flamingo_recipemd.RecipeMd',
    # ...
]
```

## Settings

``flamingo-recipemd`` can be configured with the following options:

### Extra Tags

You can add extra tags to every recipie with the ``RECIPEMD_EXTRA_TAGS``
option:
```
RECIPEMD_EXTRA_TAGS = ['Essen']
```

## TODO

* [ ] Create a pypi-package for this project
* [ ] Do proper releases
