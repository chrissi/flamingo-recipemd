#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    include_package_data=True,
    name='flamingo-recipemd',
    version='0.1.0',
    author='Chris Fiege',
    url='https://gitli.stratum0.org/chrissi/flamingo-recipemd',
    author_email='chris@tinyhost.de',
    license='Apache License 2.0',
    packages=find_packages(),
    install_requires=[
        'recipemd',
        'beautifulsoup4',
        'markdown',
        'flamingo',
    ],
)

