from bs4 import BeautifulSoup
import markdown
import recipemd.data

from flamingo.core.utils.html import process_media_links
from flamingo.core.parser import ContentParser

"""
The RecipeParser is heavily inspired by the original
flamingo Markdown parser:
https://github.com/pengutronix/flamingo/blob/master/flamingo/plugins/md.py
"""

class RecipeParser(ContentParser):
    FILE_EXTENSIONS = ['md']

    def parse(self, file_content, content):
        if not hasattr(self.context.settings, 'MARKDOWN_EXTENSIONS'):
            self.context.settings.MARKDOWN_EXTENSIONS = []

        if not hasattr(self.context.settings, 'MARKDOWN_EXTENSION_CONFIGS'):
            self.context.settings.MARKDOWN_EXTENSION_CONFIGS = {}

        if not hasattr(self.context.settings, 'RECIPEMD_EXTRA_TAGS'):
            self.context.settings.RECIPEMD_EXTRA_TAGS = []

        extensions = self.context.settings.MARKDOWN_EXTENSIONS
        extension_configs = self.context.settings.MARKDOWN_EXTENSION_CONFIGS
        extra_tags = self.context.settings.RECIPEMD_EXTRA_TAGS

        html = markdown.markdown(file_content, extensions=extensions,
                                 extension_configs=extension_configs)

        soup = BeautifulSoup(html, 'html.parser')
        for h in soup.find_all("h1"):
            h.decompose()
        content['content_body'] = str(soup)
        process_media_links(self.context, content, soup)

        recipe_parser = recipemd.data.RecipeParser()
        recipe = recipe_parser.parse(file_content)
        content['content_title'] = recipe.title
        content["tags"] = extra_tags + recipe.tags
        content["recipe"] = recipe

class RecipeMd:
    def parser_setup(self, context):
        context.parser.add_parser(RecipeParser(context))
